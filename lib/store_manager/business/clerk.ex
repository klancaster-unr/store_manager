defmodule StoreManager.Business.Clerk do
  use Ecto.Schema
  import Ecto.Changeset

  schema "clerks" do
    field :name, :string
    belongs_to :store, StoreManager.Business.Store
    timestamps()
  end

  @doc false
  def changeset(clerk, attrs) do
    clerk
    |> cast(attrs, [:name, :store_id])
    |> validate_required([:name, :store_id])

  end
end
